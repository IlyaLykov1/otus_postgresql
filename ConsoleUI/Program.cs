﻿using DataAccess.PostgreSQL;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            PopulateDb();
            ShowDbContent();

            Console.WriteLine("Do you want to add a new customer? (y/n)");
            var key = Console.ReadKey();
            if(key.Key == ConsoleKey.Y) AddCusmomerToDb();

            ShowDbContent();
        }

        private static void PopulateDb()
        {
            var customer1 = new Customer { FirstName = "Ivan", LastName = "Ivanov", Email = "ivanov@gmail.com", PhoneNumber = "+79221234560" };
            var customer2 = new Customer { FirstName = "Petr", LastName = "Petrov", Email = "petrov@gmail.com", PhoneNumber = "+79212345678" };
            var customer3 = new Customer { FirstName = "Mark", LastName = "Twen", Email = "MarkTwen@gmail.com" };
            var customer4 = new Customer { FirstName = "Lev", LastName = "Tolstoy", Email = "NoCommas@tolstoy.com" };
            var customer5 = new Customer { FirstName = "Brad", LastName = "Pitt", Email = "BradPitt@gmail.com", PhoneNumber = "+14842918792" };

            var product1 = new Product { Name = "Xiomi 5", Description = "Iphone killer", Price = 100m };
            var product2 = new Product { Name = "Milk", Description = "Natural Milk", Price = 0.9m };
            var product3 = new Product { Name = "Bread", Description = "White Bread", Price = 0.5m };
            var product4 = new Product { Name = "Lada Vesta", Description = "Russian best car", Price = 15000m };
            var product5 = new Product { Name = "Kindle PaperWhite", Description = "Amazon`s e-reader", Price = 120m };

            customer1.Orders.Add(new Order { Items = new List<OrderItem> { new OrderItem { Product = product1, Quantity = 2 } } });
            customer1.Orders.Add(new Order { Items = new List<OrderItem> { new OrderItem { Product = product5, Quantity = 1 } } });
            customer2.Orders.Add(new Order { Items = new List<OrderItem> { new OrderItem { Product = product2, Quantity = 2 } } });
            customer2.Orders.Add(new Order { Items = new List<OrderItem> { new OrderItem { Product = product3, Quantity = 1 } } });
            customer3.Orders.Add(new Order { Items = new List<OrderItem> { new OrderItem { Product = product4, Quantity = 1 } } });
            customer4.Orders.Add(new Order { Items = new List<OrderItem> { new OrderItem { Product = product5, Quantity = 1 } } });
            customer5.Orders.Add(new Order { Items = new List<OrderItem> { new OrderItem { Product = product1, Quantity = 4 } } });

            using (AppDbContext db = new AppDbContext())
            {
                db.Customers.AddRange(customer1, customer2, customer3, customer4, customer5);
                db.Products.AddRange(product1, product2, product3, product4, product5);

                db.SaveChanges();
            }
            Console.WriteLine("Write to db is finished");
        }

        private static void ShowDbContent()
        {
            using (AppDbContext db = new AppDbContext())
            {
                var customers = db.Customers.ToList();
                Console.WriteLine("Customers list:");
                foreach(var c in customers)
                {
                    Console.WriteLine($"{c.Id}.{c.LastName} {c.FirstName} {c.Email} {c.PhoneNumber}");
                }
                Console.WriteLine();

                var products = db.Products.ToList();
                Console.WriteLine("Products list:");
                foreach (var p in products)
                {
                    Console.WriteLine($"{p.Id}.{p.Name} - {p.Description} ${string.Format("{0:N2}", p.Price)}");
                }
                Console.WriteLine();

                var orderItems = db.OrderItems.ToList();
                Console.WriteLine("OrderItem list:");
                foreach (var o in orderItems)
                {
                    Console.WriteLine($"{o.Id}.{o.OrderId} {o.ProductId} {o.Quantity}");
                }
                Console.WriteLine();

                var orders = db.Orders.ToList();
                Console.WriteLine("Orders list:");
                foreach (var o in orders)
                {
                    Console.WriteLine($"{o.Id}.{o.CustomerId} {o.OrderPlacedDate} {o.Items.FirstOrDefault().Product.Name}");
                }
                Console.WriteLine();
            }
        }

        private static void AddCusmomerToDb()
        {
            Console.WriteLine();
            Console.WriteLine("Last name:");
            var lastName = Console.ReadLine();
            Console.WriteLine("First name:");
            var firstName = Console.ReadLine();
            Console.WriteLine("Email:");
            var email = Console.ReadLine();
            Console.WriteLine("PhoneNumber:");
            var phoneNumber = Console.ReadLine();

            var customer = new Customer { LastName = lastName, FirstName = firstName, Email = email, PhoneNumber = phoneNumber };

            using (AppDbContext db = new AppDbContext())
            {
                db.Customers.Add(customer);
                db.SaveChanges();
            }

            Console.WriteLine($"{lastName} {firstName} has been added.");
            Console.ReadLine();
        }
    }
}
